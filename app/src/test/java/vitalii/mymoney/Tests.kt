package vitalii.mymoney

import org.junit.Assert.assertEquals
import org.junit.Test
import vitalii.mymoney.domain.model.Exchange
import vitalii.mymoney.ui.converter.ConverterViewModel
import vitalii.mymoney.ui.search.branch.BranchesSearchViewModel

class Tests {

    private val converterViewModel = ConverterViewModel()
    private val exchange = Exchange("", "", 2.5F, 3.5F)
    private val branchesSearchViewModel = BranchesSearchViewModel(null)

    @Test
    fun atLeastOneEmpty_withEmptyData_isCorrect() {
        val actual = branchesSearchViewModel.atLeastOneNotEmpty("")
        assertEquals(false, actual)
    }

    @Test
    fun atLeastOneNotEmpty_withData_isCorrect() {
        val actual = branchesSearchViewModel.atLeastOneNotEmpty("Full", "")
        assertEquals(true, actual)
    }

    @Test
    fun getResult_isBuy_true_calculateBuy_isCorrect() {
        val amountMoney = 35F
        val actual = converterViewModel.getResult(true, amountMoney, exchange).toFloat()
        val expected = 10F
        assertEquals(expected, actual)
    }

    @Test
    fun getResult_isBuy_false_calculateSale_isCorrect() {
        val amountMoney = 10F
        val actual = converterViewModel.getResult(false, amountMoney, exchange).toFloat()
        val expected = 25F
        assertEquals(expected, actual)
    }
}