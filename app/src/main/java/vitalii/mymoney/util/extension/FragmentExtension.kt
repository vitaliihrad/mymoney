package vitalii.mymoney.util.extension

import android.widget.Toast
import androidx.fragment.app.Fragment
import vitalii.mymoney.App

fun Fragment.showToast(messageId: Int) {
    Toast.makeText(
        context,
        messageId,
        Toast.LENGTH_SHORT
    ).show()
}

fun Fragment.showToast(message: String) {
    Toast.makeText(
        context,
        message,
        Toast.LENGTH_SHORT
    ).show()
}

fun Fragment.getViewModelsFactory() = (requireContext().applicationContext as App).viewModelsFactory