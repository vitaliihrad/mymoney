package vitalii.mymoney.util.extension

fun <K, V> Map<K, V>.getOrDefaultValue(key: K, value: V): V {
    return get(key) ?: value
}