package vitalii.mymoney.util.extension

import android.content.Context
import vitalii.mymoney.R
import vitalii.mymoney.util.network.NoConnectionInterceptor
import vitalii.mymoney.util.other.ResultOf

suspend fun <T> safeCall(context: Context, networkRequest: suspend () -> List<T>) : ResultOf<List<T>> {
    return try {
        ResultOf.Success(networkRequest())
    } catch (e: NoConnectionInterceptor.NoInternetException) {
        ResultOf.Failure(e, context.getString(R.string.check_internet_connection_error))
    } catch (e: NoConnectionInterceptor.NoConnectivityException) {
        ResultOf.Failure(e, context.getString(R.string.internet_unavailable_error))
    }
}