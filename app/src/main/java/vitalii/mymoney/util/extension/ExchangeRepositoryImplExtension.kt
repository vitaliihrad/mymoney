package vitalii.mymoney.util.extension

import vitalii.mymoney.data.network.model.BranchResponse
import vitalii.mymoney.data.network.model.Devices
import vitalii.mymoney.data.network.model.ExchangeRateResponse
import vitalii.mymoney.domain.model.Branch
import vitalii.mymoney.domain.model.Exchange
import vitalii.mymoney.domain.model.Terminal

fun createBranch(branchResponse: BranchResponse) = Branch(
    branchResponse.name,
    branchResponse.state,
    branchResponse.id,
    branchResponse.country,
    branchResponse.city,
    branchResponse.index,
    branchResponse.phone,
    branchResponse.email,
    branchResponse.address
)

fun createTerminal(devices: Devices) = Terminal(
    devices.latitude,
    devices.longitude,
    devices.fullAddressRu,
    devices.placeRu,
    0.0
)

fun createExchange(it: ExchangeRateResponse) = Exchange(
    it.currency,
    it.baseCurrency,
    it.buy.toFloat(),
    it.sale.toFloat()
)