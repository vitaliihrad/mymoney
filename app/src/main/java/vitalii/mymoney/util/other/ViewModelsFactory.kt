package vitalii.mymoney.util.other

import android.content.Context
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import vitalii.mymoney.data.repo.ExchangeRepositoryImpl
import vitalii.mymoney.domain.repo.ExchangeRepository
import vitalii.mymoney.ui.exchange.ExchangeViewModel
import vitalii.mymoney.ui.search.branch.BranchesSearchViewModel
import vitalii.mymoney.ui.search.terminal.TerminalSearchViewModel

class ViewModelsFactory(context: Context) : ViewModelProvider.Factory {

    private val exchangeRepositoryImpl: ExchangeRepository = ExchangeRepositoryImpl(context)

    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        return when (modelClass) {
            ExchangeViewModel::class.java -> {
                @Suppress("UNCHECKED_CAST")
                ExchangeViewModel(exchangeRepositoryImpl) as T
            }
            TerminalSearchViewModel::class.java -> {
                @Suppress("UNCHECKED_CAST")
                TerminalSearchViewModel(exchangeRepositoryImpl) as T
            }
            BranchesSearchViewModel::class.java -> {
                @Suppress("UNCHECKED_CAST")
                BranchesSearchViewModel(exchangeRepositoryImpl) as T
            }
            else -> {
                throw Exception("ViewModel not supported")
            }
        }
    }
}