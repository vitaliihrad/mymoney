package vitalii.mymoney

import android.app.Application
import vitalii.mymoney.util.other.ViewModelsFactory

class App : Application() {

    val viewModelsFactory: ViewModelsFactory by lazy {
        ViewModelsFactory(applicationContext)
    }
}