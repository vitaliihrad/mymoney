package vitalii.mymoney.data.repo

import android.content.Context
import vitalii.mymoney.data.network.api.PrivatApiService
import vitalii.mymoney.domain.model.Branch
import vitalii.mymoney.domain.model.Exchange
import vitalii.mymoney.domain.model.Terminal
import vitalii.mymoney.domain.repo.ExchangeRepository
import vitalii.mymoney.util.extension.createBranch
import vitalii.mymoney.util.extension.createExchange
import vitalii.mymoney.util.extension.createTerminal
import vitalii.mymoney.util.extension.safeCall
import vitalii.mymoney.util.other.ResultOf

class ExchangeRepositoryImpl(val context: Context) : ExchangeRepository {

    private val privatApiService = PrivatApiService.create(context)

    override suspend fun getExchangeRates(): ResultOf<List<Exchange>> {
        return safeCall(context) {
            privatApiService.getExchangeRates().map {
                createExchange(it)
            }
        }
    }

    override suspend fun getTerminalByCity(city: String): ResultOf<List<Terminal>> {
        return safeCall(context) {
            val terminal = privatApiService.getTerminalByCity(city)
            terminal.devices.map { devices ->
                createTerminal(devices)
            }
        }
    }

    override suspend fun getBranchesByAddress(
        city: String,
        street: String
    ): ResultOf<List<Branch>> {
        return safeCall(context) {
            privatApiService.getBranchesByAddress(city, street).map { branchResponse ->
                createBranch(branchResponse)
            }
        }
    }
}