package vitalii.mymoney.data.repo

import vitalii.mymoney.domain.model.Branch
import vitalii.mymoney.domain.model.Exchange
import vitalii.mymoney.domain.model.Terminal
import vitalii.mymoney.domain.repo.ExchangeRepository
import vitalii.mymoney.util.other.ResultOf

class MockExchangeRepository : ExchangeRepository {

    override suspend fun getExchangeRates(): ResultOf<List<Exchange>> {
        return ResultOf.Success(
            listOf(
                Exchange("USD", "UAH", 26.95000F, 27.35000F),
                Exchange("EUR", "UAH", 30.35000F, 30.95000F),
                Exchange("RUR", "UAH", 0.36000F, 0.39000F),
                Exchange("BTC", "USD", 47346.6194F, 52330.4740F)
            )
        )
    }

    override suspend fun getTerminalByCity(city: String): ResultOf<List<Terminal>> {
        return ResultOf.Success(
            listOf(
                Terminal(0.0, 0.0, "", "", 0.0)
            )
        )
    }

    override suspend fun getBranchesByAddress(city: String, street: String): ResultOf<List<Branch>> {
        return ResultOf.Success(
            listOf(
                Branch("", "", 0, "", "", 0, "", "", "")
            )
        )
    }
}