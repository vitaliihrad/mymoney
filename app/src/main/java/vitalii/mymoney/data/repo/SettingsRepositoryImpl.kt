package vitalii.mymoney.data.repo

import android.content.Context
import androidx.appcompat.app.AppCompatDelegate
import vitalii.mymoney.domain.repo.SettingsRepository

class SettingsRepositoryImpl : SettingsRepository {

    override fun saveTheme(theme: Int, context: Context) {
        val sharedPreferences = context.getSharedPreferences(THEME_KEY, Context.MODE_PRIVATE)
            .edit()
        sharedPreferences.putInt(THEME_KEY, theme)
            .apply()
    }

    override fun loadTheme(context: Context): Int {
        val sharedPreferences = context.getSharedPreferences(THEME_KEY, Context.MODE_PRIVATE)
        return sharedPreferences.getInt(
            THEME_KEY,
            AppCompatDelegate.MODE_NIGHT_FOLLOW_SYSTEM
        )
    }

    companion object {
        const val THEME_KEY = "themesSettings"
    }
}