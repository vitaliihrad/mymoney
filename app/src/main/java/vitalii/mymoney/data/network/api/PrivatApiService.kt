package vitalii.mymoney.data.network.api

import android.content.Context
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Query
import vitalii.mymoney.data.network.model.BranchResponse
import vitalii.mymoney.data.network.model.ExchangeRateResponse
import vitalii.mymoney.data.network.model.TerminalResponse
import vitalii.mymoney.util.network.NoConnectionInterceptor


interface PrivatApiService {

    @GET("infrastructure?json&tso")
    suspend fun getTerminalByCity(@Query("city") city: String): TerminalResponse

    @GET("pboffice?json")
    suspend fun getBranchesByAddress(
        @Query("city") city: String,
        @Query("address") address: String
    ): Collection<BranchResponse>

    @GET("pubinfo?json&exchange&coursid=5")
    suspend fun getExchangeRates(): List<ExchangeRateResponse>

    companion object {
        private const val BASE_URL = "https://api.privatbank.ua/p24api/"

        fun create(context: Context): PrivatApiService {
            val logging = HttpLoggingInterceptor()
            logging.setLevel(HttpLoggingInterceptor.Level.BASIC)
            val noConnectionInterceptor = NoConnectionInterceptor(context)
            val client: OkHttpClient = OkHttpClient.Builder()
                .addInterceptor(logging)
                .addInterceptor(noConnectionInterceptor)
                .build()
            return Retrofit.Builder()
                .client(client)
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build()
                .create(PrivatApiService::class.java)
        }
    }
}