package vitalii.mymoney.data.network.model

import com.google.gson.annotations.SerializedName

data class BranchResponse(
    @SerializedName("name") val name : String,
    @SerializedName("state") val state : String,
    @SerializedName("id") val id : Int,
    @SerializedName("country") val country : String,
    @SerializedName("city") val city : String,
    @SerializedName("index") val index : Int,
    @SerializedName("phone") val phone : String,
    @SerializedName("email") val email : String,
    @SerializedName("address") val address : String
)