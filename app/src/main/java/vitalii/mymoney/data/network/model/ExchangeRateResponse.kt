package vitalii.mymoney.data.network.model

import com.google.gson.annotations.SerializedName

data class ExchangeRateResponse(
    @SerializedName("ccy") val currency: String,
    @SerializedName("base_ccy") val baseCurrency: String,
    @SerializedName("buy") val buy: Double,
    @SerializedName("sale") val sale: Double
)