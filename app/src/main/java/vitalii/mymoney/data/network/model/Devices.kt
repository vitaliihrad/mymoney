package vitalii.mymoney.data.network.model

import com.google.gson.annotations.SerializedName

data class Devices (
    @SerializedName("latitude") val latitude : Double,
    @SerializedName("longitude") val longitude : Double,
    @SerializedName("fullAddressRu") val fullAddressRu : String,
    @SerializedName("placeRu") val placeRu : String,
)