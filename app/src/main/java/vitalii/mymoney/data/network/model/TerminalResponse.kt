package vitalii.mymoney.data.network.model

import com.google.gson.annotations.SerializedName

data class TerminalResponse(
    @SerializedName("devices") val devices : List<Devices>
)