package vitalii.mymoney.domain.model

class Terminal(
    val latitude: Double,
    val longitude: Double,
    val fullAddressRu: String,
    val placeRu: String,
    var distance: Double
)