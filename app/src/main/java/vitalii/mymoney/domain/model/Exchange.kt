package vitalii.mymoney.domain.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
class Exchange(
    val currency: String,
    val baseCurrency: String,
    val buy: Float,
    val sale: Float
) : Parcelable