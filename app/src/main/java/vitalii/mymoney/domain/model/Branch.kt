package vitalii.mymoney.domain.model

class Branch(
    val name : String,
    val state : String,
    val id : Int,
    val country : String,
    val city : String,
    val index : Int,
    val phone : String,
    val email : String,
    val address : String
)