package vitalii.mymoney.domain.repo

import vitalii.mymoney.domain.model.Branch
import vitalii.mymoney.domain.model.Exchange
import vitalii.mymoney.domain.model.Terminal
import vitalii.mymoney.util.other.ResultOf

interface ExchangeRepository {

    suspend fun getExchangeRates(): ResultOf<List<Exchange>>

    suspend fun getTerminalByCity(city: String): ResultOf<List<Terminal>>

    suspend fun getBranchesByAddress(city: String, street: String): ResultOf<List<Branch>>
}