package vitalii.mymoney.domain.repo

import android.location.Location
import androidx.lifecycle.LiveData

interface LocationHelperRepository {

    val locationLiveData: LiveData<Location>

    fun removeUpdateLocationManager()

    fun setRequestLocationUpdate()
}