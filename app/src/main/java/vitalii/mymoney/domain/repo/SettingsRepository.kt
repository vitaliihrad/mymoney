package vitalii.mymoney.domain.repo

import android.content.Context

interface SettingsRepository {

    fun saveTheme(theme: Int, context: Context)

    fun loadTheme(context: Context): Int
}