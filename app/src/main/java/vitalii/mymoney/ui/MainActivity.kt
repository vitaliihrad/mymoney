package vitalii.mymoney.ui

import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.app.AppCompatDelegate
import vitalii.mymoney.R
import vitalii.mymoney.ui.exchange.ExchangeFragment

class MainActivity : AppCompatActivity() {

    private val mainActivityViewModel by viewModels<MainActivityViewModel>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        AppCompatDelegate.setDefaultNightMode(mainActivityViewModel.loadTheme(this))
        mainActivityViewModel.settingsThemesLiveData.observe(this) { theme ->
            AppCompatDelegate.setDefaultNightMode(theme)
        }
        if (savedInstanceState == null) {
            supportFragmentManager.beginTransaction()
                .replace(
                    R.id.main_container,
                    ExchangeFragment.newInstance(),
                    ExchangeFragment.TAG
                )
                .commit()
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.settings_toll_bar, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        selectTheme(item)
        return super.onOptionsItemSelected(item)
    }

    private fun selectTheme(item: MenuItem) {
        mainActivityViewModel.saveTheme(
            when (item.itemId) {
                R.id.light_theme -> AppCompatDelegate.MODE_NIGHT_NO
                R.id.night_theme -> AppCompatDelegate.MODE_NIGHT_YES
                else -> AppCompatDelegate.MODE_NIGHT_FOLLOW_SYSTEM
            },
            this
        )
    }
}