package vitalii.mymoney.ui

import android.content.Context
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import vitalii.mymoney.data.repo.SettingsRepositoryImpl
import vitalii.mymoney.domain.repo.SettingsRepository

class MainActivityViewModel : ViewModel() {

    private val _settingsThemesLiveData = MutableLiveData<Int>()
    val settingsThemesLiveData: LiveData<Int> = _settingsThemesLiveData
    private val themeSettingsRepository: SettingsRepository = SettingsRepositoryImpl()

    fun saveTheme(theme: Int, context: Context) {
        themeSettingsRepository.saveTheme(theme, context)
        _settingsThemesLiveData.postValue(theme)
    }

    fun loadTheme(context: Context) = themeSettingsRepository.loadTheme(context)
}