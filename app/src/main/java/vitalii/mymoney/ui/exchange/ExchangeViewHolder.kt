package vitalii.mymoney.ui.exchange

import android.view.View
import vitalii.mymoney.databinding.ItemExchangeReateBinding
import vitalii.mymoney.domain.model.Exchange
import vitalii.mymoney.ui.base.viewholder.BaseViewHolder

class ExchangeViewHolder(
    view: View,
    onClick: (position: Int) -> Unit
) : BaseViewHolder<Exchange>(view) {

    private val binding = ItemExchangeReateBinding.bind(itemView)
    private val currencyTextView = binding.currencyTextView
    private val buyTextView = binding.buyTextView
    private val saleTextView = binding.saleTextView

    init {
        view.setOnClickListener {
            onClick(adapterPosition)
        }
    }

    override fun bind(data: Exchange) {
        currencyTextView.text = data.run {
            "$currency / $baseCurrency"
        }
        buyTextView.text = data.buy.toString()
        saleTextView.text = data.sale.toString()
    }
}