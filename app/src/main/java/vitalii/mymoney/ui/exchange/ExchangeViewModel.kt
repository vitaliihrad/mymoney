package vitalii.mymoney.ui.exchange

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import vitalii.mymoney.domain.model.Exchange
import vitalii.mymoney.domain.repo.ExchangeRepository
import vitalii.mymoney.util.other.ResultOf

class ExchangeViewModel(private val exchangeRepository: ExchangeRepository) : ViewModel() {

    private val _exchangeRatesListLiveData = MutableLiveData<ResultOf<List<Exchange>>>()
    val exchangeRatesListLiveData: LiveData<ResultOf<List<Exchange>>> = _exchangeRatesListLiveData

    fun getExchangeRates() {
        viewModelScope.launch(Dispatchers.IO) {
            _exchangeRatesListLiveData.postValue(exchangeRepository.getExchangeRates())
        }
    }
}