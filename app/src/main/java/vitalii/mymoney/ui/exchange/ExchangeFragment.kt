package vitalii.mymoney.ui.exchange

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.fragment.app.commit
import androidx.fragment.app.viewModels
import vitalii.mymoney.R
import vitalii.mymoney.databinding.FragmentExchangeBinding
import vitalii.mymoney.ui.converter.ConverterFragment
import vitalii.mymoney.ui.search.branch.BranchesSearchFragment
import vitalii.mymoney.ui.search.terminal.TerminalSearchFragment
import vitalii.mymoney.util.extension.getViewModelsFactory
import vitalii.mymoney.util.extension.showToast
import vitalii.mymoney.util.other.ResultOf

class ExchangeFragment : Fragment() {

    private lateinit var binding: FragmentExchangeBinding
    private val exchangeViewModel by viewModels<ExchangeViewModel> {
        getViewModelsFactory()
    }
    private val onItemClickListener: (position: Int) -> Unit = { position ->
        val bundle = bundleOf(ConverterFragment.BUNDLE_KEY_EXCHANGE to adapter.getItem(position))
        parentFragmentManager.commit {
            replace(
                R.id.main_container,
                ConverterFragment::class.java,
                bundle,
                ConverterFragment.TAG
            )
            addToBackStack(ConverterFragment.TAG)
        }
    }
    private val adapter = ExchangeAdapter(onItemClickListener)

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        exchangeViewModel.getExchangeRates()
        binding = FragmentExchangeBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setObserving()
        binding.cashRateRecyclerView.adapter = adapter
        setListeners()
    }

    private fun setObserving() {
        exchangeViewModel.exchangeRatesListLiveData.observe(viewLifecycleOwner) { result ->
            @Suppress("UNCHECKED_CAST")
            when (result) {
                is ResultOf.Success -> adapter.submit(result.value)
                is ResultOf.Failure -> result.message?.let { showToast(it) }
            }
        }
    }

    private fun setListeners() {
        binding.apply {
            atmSearchButton.setOnClickListener {
                parentFragmentManager.commit {
                    replace(
                        R.id.main_container,
                        TerminalSearchFragment.newInstance(),
                        TerminalSearchFragment.TAG
                    )
                    addToBackStack(TerminalSearchFragment.TAG)
                }
            }
            branchSearchButton.setOnClickListener {
                parentFragmentManager.commit {
                    replace(
                        R.id.main_container,
                        BranchesSearchFragment.newInstance(),
                        BranchesSearchFragment.TAG
                    )
                    addToBackStack(BranchesSearchFragment.TAG)
                }
            }
        }
    }

    companion object {

        val TAG: String = ExchangeFragment::class.java.name

        fun newInstance() = ExchangeFragment()
    }
}
