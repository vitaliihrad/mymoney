package vitalii.mymoney.ui.exchange

import android.view.View
import vitalii.mymoney.R
import vitalii.mymoney.domain.model.Exchange
import vitalii.mymoney.ui.base.adapter.BaseAdapter
import vitalii.mymoney.ui.base.viewholder.BaseViewHolder

class ExchangeAdapter(
    private val onClick: (position: Int) -> Unit
) : BaseAdapter<Exchange>() {

    override fun getViewHolder(view: View): BaseViewHolder<Exchange> =
        ExchangeViewHolder(view, onClick)

    override fun getItemRes() = R.layout.item_exchange_reate
}