package vitalii.mymoney.ui.converter

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import vitalii.mymoney.R
import vitalii.mymoney.databinding.FragmentConverterBinding
import vitalii.mymoney.domain.model.Exchange
import vitalii.mymoney.util.extension.showToast

class ConverterFragment : Fragment() {

    private val converterViewModel by viewModels<ConverterViewModel>()
    private val exchange: Exchange by lazy {
        requireNotNull(requireArguments().getParcelable(BUNDLE_KEY_EXCHANGE))
    }
    private var isBuy: Boolean = false
    private lateinit var binding: FragmentConverterBinding

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentConverterBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setClickListener()
        setStartValueTextView(binding.baseCurrencyTextView, binding.mainCurrencyTextView)
    }

    private fun setClickListener() {
        binding.apply {
            buySaleBtn.setOnClickListener {
                isBuy = !isBuy
                if (isBuy) {
                    setStartValueTextView(mainCurrencyTextView, baseCurrencyTextView)
                } else {
                    setStartValueTextView(baseCurrencyTextView, mainCurrencyTextView)
                }
            }

            calculateBtn.setOnClickListener {
                val amountMoney = amountMoneyTextInputEditText.text.toString()
                if (amountMoney.isNotEmpty()) {
                    resultTextInputEditText.setText(
                        converterViewModel.getResult(isBuy, amountMoney.toFloat(), exchange)
                    )
                } else {
                    showToast(R.string.error_enter_data_toast_text)
                }
            }
        }
    }

    private fun setStartValueTextView(baseCurrency: TextView, mainCurrency: TextView) {
        binding.apply {
            baseCurrency.text = exchange.currency
            mainCurrency.text = exchange.baseCurrency
            rateTextView.text =
                getString(R.string.rate, exchange.currency, checkIsBuy(), exchange.baseCurrency)
        }
    }

    private fun checkIsBuy(): String = if (!isBuy) {
        exchange.buy.toString()
    } else {
        exchange.sale.toString()
    }

    companion object {

        const val BUNDLE_KEY_EXCHANGE = "Exc"
        val TAG: String = ConverterFragment::class.java.name
    }
}