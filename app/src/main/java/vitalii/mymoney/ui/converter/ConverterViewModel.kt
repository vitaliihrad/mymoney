package vitalii.mymoney.ui.converter

import androidx.lifecycle.ViewModel
import vitalii.mymoney.domain.model.Exchange

class ConverterViewModel : ViewModel() {

    fun getResult(isBuy: Boolean, amountMoney: Float, exchange: Exchange) = if (isBuy) {
        calculateBuy(amountMoney, exchange.sale).toString()
    } else {
        calculateSale(amountMoney, exchange.buy).toString()
    }

    private fun calculateBuy(amountMoney: Float, price: Float) = amountMoney / price

    private fun calculateSale(amountMoney: Float, price: Float) = amountMoney * price
}