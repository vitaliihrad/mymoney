package vitalii.mymoney.ui.search.terminal

import android.view.View
import vitalii.mymoney.databinding.ItemTerminalBinding
import vitalii.mymoney.domain.model.Terminal
import vitalii.mymoney.ui.base.viewholder.BaseViewHolder

class TerminalSearchViewHolder(view: View): BaseViewHolder<Terminal>(view) {

    private val binding = ItemTerminalBinding.bind(itemView)
    private val addressTextView = binding.fullAddressTextView
    private val distanceTextView = binding.distanceTextView
    private val placeTextView = binding.placeTextView

    override fun bind(data: Terminal) {
        addressTextView.text = data.fullAddressRu
        placeTextView.text = data.placeRu
        distanceTextView.text =data.distance.toString()
    }
}