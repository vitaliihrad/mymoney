package vitalii.mymoney.ui.search.branch

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import vitalii.mymoney.domain.model.Branch
import vitalii.mymoney.domain.repo.ExchangeRepository
import vitalii.mymoney.util.other.ResultOf

class BranchesSearchViewModel(private val branchRepository: ExchangeRepository?) : ViewModel() {

    private val _branchesListLiveData = MutableLiveData<ResultOf<List<Branch>>>()
    val branchesListLiveData: LiveData<ResultOf<List<Branch>>> = _branchesListLiveData

    fun getBranchesByAddress(city: String, street: String) {
        viewModelScope.launch(Dispatchers.IO) {
            _branchesListLiveData.postValue(branchRepository?.getBranchesByAddress(city, street))
        }
    }

    fun atLeastOneNotEmpty(vararg data: String): Boolean {
        return data.any {
            it.isNotEmpty()
        }
    }
}