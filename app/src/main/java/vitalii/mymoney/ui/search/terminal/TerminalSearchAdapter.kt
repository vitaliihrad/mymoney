package vitalii.mymoney.ui.search.terminal

import android.view.View
import vitalii.mymoney.ui.base.adapter.BaseAdapter
import vitalii.mymoney.R
import vitalii.mymoney.domain.model.Terminal

class TerminalSearchAdapter : BaseAdapter<Terminal>() {

    override fun getViewHolder(view: View) = TerminalSearchViewHolder(view)

    override fun getItemRes() = R.layout.item_terminal
}



