package vitalii.mymoney.ui.search.terminal

import android.Manifest
import android.app.AlertDialog
import android.content.Intent
import android.os.Bundle
import android.provider.Settings
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.activity.result.contract.ActivityResultContracts
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import kotlinx.android.synthetic.main.fragment_terminal_search.*
import vitalii.mymoney.R
import vitalii.mymoney.data.repo.LocationHelperRepositoryImpl
import vitalii.mymoney.databinding.FragmentTerminalSearchBinding
import vitalii.mymoney.domain.repo.LocationHelperRepository
import vitalii.mymoney.util.extension.getViewModelsFactory
import vitalii.mymoney.util.extension.getOrDefaultValue
import vitalii.mymoney.util.extension.showToast
import vitalii.mymoney.util.other.ResultOf

class TerminalSearchFragment : Fragment() {

    private lateinit var binding: FragmentTerminalSearchBinding
    private val terminalSearchViewModel by viewModels<TerminalSearchViewModel> {
        getViewModelsFactory()
    }
    private val locationHelperRepository: LocationHelperRepository by lazy {
        LocationHelperRepositoryImpl(requireContext())
    }

    private val adapter = TerminalSearchAdapter()

    private val locationPermissionRequest = registerForActivityResult(
        ActivityResultContracts.RequestMultiplePermissions()
    ) { permissions ->
        when {
            permissions.getOrDefaultValue(Manifest.permission.ACCESS_FINE_LOCATION, false) -> {
                locationHelperRepository.setRequestLocationUpdate()
            }
            permissions.getOrDefaultValue(Manifest.permission.ACCESS_COARSE_LOCATION, false) -> {
                locationHelperRepository.setRequestLocationUpdate()
            }
            else -> {
                isAccessDenied()
            }
        }
    }
    private var isSorted: Boolean = false

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        terminalSearchViewModel.getTerminalByCity(getString(R.string.base_city_terminal_search))
        binding = FragmentTerminalSearchBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setDataLoading(true)
        setObserving()
        binding.terminalsRecyclerView.adapter = adapter
        setListeners()
    }

    private fun setObserving() {
        locationHelperRepository.locationLiveData.observe(viewLifecycleOwner) { location ->
            terminalSearchViewModel.showDistance(location)
        }
        terminalSearchViewModel.terminalListLiveData.observe(viewLifecycleOwner) { result ->
            @Suppress("UNCHECKED_CAST")
            when (result) {
                is ResultOf.Success -> {
                    checkPositionCalculateDistanceSwitch()
                    adapter.submit(result.value)
                }
                is ResultOf.Failure -> result.message?.let { showToast(it) }
            }
            setDataLoading(false)
        }
    }

    private fun checkPositionCalculateDistanceSwitch() {
        if (binding.calculateDistanceSwitch.isChecked && !isSorted) {
            isSorted = true
            terminalSearchViewModel.showDistance(requireNotNull(locationHelperRepository.locationLiveData.value))
        }
    }

    private fun setDataLoading(isStarted: Boolean) {
        binding.also {
            if (isStarted) {
                terminals_recycler_view.isVisible = false
                loading_progress_bar.isVisible = true
            } else {
                terminals_recycler_view.isVisible = true
                loading_progress_bar.isVisible = false
            }
        }
    }

    private fun setListeners() {
        binding.apply {
            searchButton.setOnClickListener {
                setDataLoading(true)
                terminalSearchViewModel.getTerminalByCity(cityInputEditText.text.toString())
                isSorted = false
            }

            calculateDistanceSwitch.setOnClickListener {
                setCalculationSwitchPosition()
            }
        }
    }

    private fun setCalculationSwitchPosition() {
        binding.apply {
            if (calculateDistanceSwitch.isChecked) {
                setDataLoading(true)
                showShouldRationale()
            }
        }
    }

    private fun showShouldRationale() {
        if (shouldShowRequestPermissionRationale(Manifest.permission.ACCESS_FINE_LOCATION)
            || shouldShowRequestPermissionRationale(Manifest.permission.ACCESS_COARSE_LOCATION)
        ) {
            userNotificationDialog(
                getString(R.string.warning_dialog),
                getString(R.string.message_dialog)
            )
        } else {
            requestLocation()
        }
    }

    private fun userNotificationDialog(title: String, message: String) {
        val builder = AlertDialog.Builder(context)
        builder.setTitle(title)
        builder.setMessage(message)
        builder.setPositiveButton(getString(R.string.text_alert_dialog_positive_btn)) { _, _: Int ->
            requestLocation()
        }
        builder.show()
    }

    private fun requestLocation() {
        locationPermissionRequest.launch(
            arrayOf(
                Manifest.permission.ACCESS_FINE_LOCATION,
                Manifest.permission.ACCESS_COARSE_LOCATION
            )
        )
    }

    private fun isAccessDenied() {
        val builder = AlertDialog.Builder(context)
        builder.setTitle(getString(R.string.warning_dialog))
        builder.setMessage(getString(R.string.access_denied_message_alert_dialog))
        builder.setPositiveButton(getString(R.string.give_access_alert_dialog_positive_btn)) { _, _ ->
            startActivity(Intent(Settings.ACTION_APPLICATION_SETTINGS))
        }
        builder.setNegativeButton(getString(R.string.denied_access_alert_dialog_negative_btn)) { _, _ ->
            binding.apply {
                calculateDistanceSwitch.isChecked = false
            }
        }
        builder.show()
    }

    companion object {

        val TAG: String = TerminalSearchFragment::class.java.name

        fun newInstance() = TerminalSearchFragment()
    }
}