package vitalii.mymoney.ui.search.terminal

import android.location.Location
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import vitalii.mymoney.domain.model.Terminal
import vitalii.mymoney.domain.repo.ExchangeRepository
import vitalii.mymoney.util.other.ResultOf
import java.math.RoundingMode
import java.text.DecimalFormat

typealias ResultOfTerminals = ResultOf<Collection<Terminal>>

class TerminalSearchViewModel(private val terminalRepository: ExchangeRepository) : ViewModel() {

    private val _terminalListLiveData = MutableLiveData<ResultOfTerminals>()
    val terminalListLiveData: LiveData<ResultOfTerminals> = _terminalListLiveData
    private val location = Location("")
    private val df = DecimalFormat("#.###")

    fun getTerminalByCity(city: String) {
        viewModelScope.launch(Dispatchers.IO) {
            _terminalListLiveData.postValue(terminalRepository.getTerminalByCity(city))
        }
    }

    fun showDistance(userLocation: Location) {
        val terminals = getTerminals()
        if (!getTerminals().isNullOrEmpty()) {
            terminals.map {
                it.distance = calculateDistance(userLocation, it.latitude, it.longitude)
            }
            _terminalListLiveData.postValue(ResultOf.Success(sortByDistance(terminals)))
        }
    }

    private fun getTerminals(): Collection<Terminal> {
        @Suppress("UNCHECKED_CAST")
        return when (val resultOf = terminalListLiveData.value) {
            is ResultOf.Success -> resultOf.value
            else -> emptyList()
        }
    }

    private fun sortByDistance(terminalsList: Collection<Terminal>): Collection<Terminal> {
        return terminalsList.sortedBy {
            it.distance
        }
    }

    private fun calculateDistance(
        userLocation: Location,
        latitude: Double,
        longitude: Double
    ): Double {
        location.longitude = latitude
        location.latitude = longitude
        df.roundingMode = RoundingMode.CEILING
        return df.format(userLocation.distanceTo(location) / 1000).toDouble()
    }
}