package vitalii.mymoney.ui.search.branch

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import vitalii.mymoney.R
import vitalii.mymoney.databinding.FragmentBranchesSearchBinding
import vitalii.mymoney.util.extension.getViewModelsFactory
import vitalii.mymoney.util.extension.showToast
import vitalii.mymoney.util.other.ResultOf

class BranchesSearchFragment : Fragment() {

    private lateinit var binding: FragmentBranchesSearchBinding
    private val branchesSearchViewModel by viewModels<BranchesSearchViewModel> {
        getViewModelsFactory()
    }
    private val adapter = BranchesSearchAdapter()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        branchesSearchViewModel.getBranchesByAddress(
            "",
            getString(R.string.base_street_branches_search)
        )

        binding = FragmentBranchesSearchBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setObserving()
        binding.branchesRecyclerView.adapter = adapter
        setListeners()
    }

    private fun setObserving() {
        branchesSearchViewModel.branchesListLiveData.observe(viewLifecycleOwner) { result ->
            @Suppress("UNCHECKED_CAST")
            when (result) {
                is ResultOf.Success -> adapter.submit(result.value)
                is ResultOf.Failure -> result.message?.let { showToast(it) }
            }
        }
    }

    private fun setListeners() {
        binding.apply {
            branchesSearchButton.setOnClickListener {
                val city = cityTextInputEditText.text.toString()
                val street = streetTextInputEditText.text.toString()
                if (branchesSearchViewModel.atLeastOneNotEmpty(city, street)) {
                    branchesSearchViewModel.getBranchesByAddress(city, street)
                } else {
                    showToast(R.string.error_enter_data_toast_text)
                }
            }
        }
    }

    companion object {
        val TAG: String = BranchesSearchFragment::class.java.name

        fun newInstance() = BranchesSearchFragment()
    }
}