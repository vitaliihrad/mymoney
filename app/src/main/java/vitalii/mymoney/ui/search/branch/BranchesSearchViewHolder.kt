package vitalii.mymoney.ui.search.branch

import android.view.View
import vitalii.mymoney.databinding.ItemBranchBinding
import vitalii.mymoney.domain.model.Branch
import vitalii.mymoney.ui.base.viewholder.BaseViewHolder

class BranchesSearchViewHolder(view: View) : BaseViewHolder<Branch>(view) {

    private val binding = ItemBranchBinding.bind(itemView)
    private val nameTextView = binding.nameTextView
    private val countryTextView = binding.countryTextView
    private val regionTextView = binding.regionTextView
    private val cityTextView = binding.cityTextView
    private val streetTextView = binding.streetTextView
    private val indexTextView = binding.indexTextView
    private val phoneTextureView = binding.phoneTextView
    private val emailTextView = binding.emailTextView

    override fun bind(data: Branch) {
        nameTextView.text = data.name
        countryTextView.text = data.country
        regionTextView.text = data.state
        cityTextView.text = data.city
        streetTextView.text = data.address
        indexTextView.text = data.index.toString()
        phoneTextureView.text = data.phone
        emailTextView.text = data.email
    }
}