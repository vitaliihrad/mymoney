package vitalii.mymoney.ui.search.branch

import android.view.View
import vitalii.mymoney.R
import vitalii.mymoney.domain.model.Branch
import vitalii.mymoney.ui.base.adapter.BaseAdapter

class BranchesSearchAdapter : BaseAdapter<Branch>() {

    override fun getViewHolder(view: View) = BranchesSearchViewHolder(view)

    override fun getItemRes() = R.layout.item_branch
}