package vitalii.mymoney.ui.base.viewholder

import android.view.View
import androidx.recyclerview.widget.RecyclerView

abstract class BaseViewHolder<T>(
    view: View,
    private val onClick: ((position: Int) -> Unit)? = null
) : RecyclerView.ViewHolder(view) {

    abstract fun bind(data: T)
}